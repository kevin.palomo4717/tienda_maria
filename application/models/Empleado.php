<?php
class Empleado extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }
  //funcion para insertar un instructor un instructor en mysql
  function insertar($datos){

  return $this->db->insert("empleado",$datos);
  }
  //FUNCION PARA CONSULTAR Instructores
  function obtenerTodos(){
    $listadoEmpleados=$this->db->get("empleado");
    if($listadoEmpleados->num_rows()>0){//SI HAY DATOOOOOS
      return $listadoEmpleados->result();
    }else {
      return false;
    }
  }
  //BORRAR Instructores
  function borrar($id_empleado){
    //DELTE FROM INSTRUCTOR WHERE id_ins
    $this->db->where("id_empleado",$id_empleado);
    if ($this->db->delete("empleado")) {
      return true;
    } else {
      return false;
    }

  }
} //CIERRE DE LA CLASE
 ?>
