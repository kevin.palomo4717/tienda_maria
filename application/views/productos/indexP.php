<h1>Listado de Productos</h1>
<?php if ($productos): ?>
  <table class="table table-striped table-bordered table-hover" style="color:black;">
    <thead>
      <tr>
        <th>ID</th>
        <th>NOMBRE Producto</th>
        <th>Fecha de Caducidad</th>
        <th>Precio Producto</th>
        <th>Tamaño Producto</th>
        <th>Disponibilidad Producto</th>
        <th>Falta Producto</th>
        <th>ACIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($productos as $filaTemporal): ?>
        <tr>
          <td>
            <?php echo $filaTemporal->id_pro; ?>
          </td>
          <td>
            <?php echo $filaTemporal->nom_pro; ?>
          </td>
          <td>
            <?php echo $filaTemporal->caducidad_pro; ?>
          </td>
          <td>
            <?php echo $filaTemporal->precio_pro; ?>
          </td>
          <td>
            <?php echo $filaTemporal->tamaño_pro; ?>
          </td>
          <td>
            <?php echo $filaTemporal->dispo_pro; ?>
          </td>
          <td>
            <?php echo $filaTemporal->falta_pro; ?>
          </td>
          <td class="text-center">
            <a href="#" title="Editar Producto">
              <i class="glyphicon glyphicon-pencil"></i>
            </a>
            &nbsp; &nbsp; &nbsp;
            <a href="<?php echo site_url(); ?>/productos/eliminar/<?php echo $filaTemporal->id_pro; ?>" title="Eliminar Producto" style="color:red;">
              <i class="glyphicon glyphicon-trash"></i>
            </a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>

<?php else: ?>
  <h1>NO HAY DATOS</h1>
<?php endif; ?>
