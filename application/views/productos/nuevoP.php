<h1>NUEVO Producto</h1>
<form class="" action="<?php echo site_url(); ?>/productos/guardar" method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Nombre Producto</label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre del Producto"
          class="form-control"
          name="nom_pro" value=""
          id="nom_pro">

      </div>
      <div class="col-md-4">
          <label for="">Fecha Caducidad:</label>
          <br>
          <input type="number"
          placeholder="Ingrese la fecha de caducidad"
          class="form-control"
          name="caducidad_pro" value=""
          id="caducidad_pro">
      </div>
      <div class="col-md-4">
        <label for="">Precio Producto</label>
        <br>
        <input type="number"
        placeholder="Ingrese el precio"
        class="form-control"
        name="precio_pro" value=""
        id="precio_pro">
      </div>
    </div>
    <br>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Tamaño del Producto</label>
          <br>
          <input type="text"
          placeholder="Ingrese el tamaño del cliente"
          class="form-control"
          name="tamaño_pro" value=""
          id="tamaño_pro">

      </div>
      <div class="col-md-4">
          <label for="">Disponible:</label>
          <br>
          <input type="text"
          placeholder="Ingrese esta disponible"
          class="form-control"
          name="dispo_pro" value=""
          id="dispo_pro">
      </div>
      <div class="col-md-4">
        <label for="">Falta Producto</label>
        <br>
        <input type="text"
        placeholder="Ingrese si falta Producto"
        class="form-control"
        name="falta_pro" value=""
        id="falta_pro">
      </div>
    </div>
      <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/productos/indexP"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>
