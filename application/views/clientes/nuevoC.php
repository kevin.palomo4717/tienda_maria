<h1>NUEVO Cliente</h1>
<form class="" action="<?php echo site_url(); ?>/clientes/guardar" method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Nombre Cliente</label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre del Cliente"
          class="form-control"
          name="nom_cli" value=""
          id="nom_cli">

      </div>
      <div class="col-md-4">
          <label for="">Apellido del CLiente:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el apellido del Cliente"
          class="form-control"
          name="ape_cli" value=""
          id="ape_cli">
      </div>
      <div class="col-md-4">
        <label for="">Cedula del cliente</label>
        <br>
        <input type="number"
        placeholder="Ingrese la cedula del Cliente"
        class="form-control"
        name="cedula_cli" value=""
        id="cedula_cli">
      </div>
    </div>
    <br>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Celular del cliente</label>
          <br>
          <input type="number"
          placeholder="Ingrese el numero del cliente"
          class="form-control"
          name="celu_cli" value=""
          id="celu_cli">

      </div>
      <div class="col-md-4">
          <label for="">Correo del cliente:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el correo del cliente"
          class="form-control"
          name="correo_cli" value=""
          id="correo_cli">
      </div>
      <div class="col-md-4">
        <label for="">Pago del cliente</label>
        <br>
        <input type="text"
        placeholder="Ingrese la forma de Pago"
        class="form-control"
        name="pago_cli" value=""
        id="pago_cli">
      </div>
    </div>
      <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/clientes/indexC"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>
