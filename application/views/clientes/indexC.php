<h1>Listado de Clientes</h1>
<?php if ($clientes): ?>
  <table class="table table-striped table-bordered table-hover" style="color:black;">
    <thead>
      <tr>
        <th>ID</th>
        <th>NOMBRE Cliente</th>
        <th>Apellido Cliente</th>
        <th>Cedula Cliente</th>
        <th>Celular Cliente</th>
        <th>Correo cliente</th>
        <th>Pago cliente</th>
        <th>ACIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($clientes as $filaTemporal): ?>
        <tr>
          <td>
            <?php echo $filaTemporal->id_cli; ?>
          </td>
          <td>
            <?php echo $filaTemporal->nom_cli; ?>
          </td>
          <td>
            <?php echo $filaTemporal->ape_cli; ?>
          </td>
          <td>
            <?php echo $filaTemporal->cedula_cli; ?>
          </td>
          <td>
            <?php echo $filaTemporal->celu_cli; ?>
          </td>
          <td>
            <?php echo $filaTemporal->correo_cli; ?>
          </td>
          <td>
            <?php echo $filaTemporal->pago_cli; ?>
          </td>
          <td class="text-center">
            <a href="#" title="Editar Cliente">
              <i class="glyphicon glyphicon-pencil"></i>
            </a>
            &nbsp; &nbsp; &nbsp;
            <a href="<?php echo site_url(); ?>/clientes/eliminar/<?php echo $filaTemporal->id_cli; ?>" title="Eliminar Cliente" style="color:red;">
              <i class="glyphicon glyphicon-trash"></i>
            </a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>

<?php else: ?>
  <h1>NO HAY DATOS</h1>
<?php endif; ?>
