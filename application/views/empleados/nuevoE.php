<h1>NUEVO EMPLEADO</h1>
<form class=""
action="<?php echo site_url(); ?>/empleados/guardar"
method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Cédula:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre del empleado"
          class="form-control"
          name="nom_emp" value=""
          id="nom_emp">

      </div>
      <div class="col-md-4">
          <label for="">Apellido de Empleado:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el apellido del empleado"
          class="form-control"
          name="ape_emp" value=""
          id="ape_emp">
      </div>
      <div class="col-md-4">
        <label for="">Genero del empleado:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el genero del empleado"
        class="form-control"
        name="genero_emp" value=""
        id="genero_emp">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Celular del empleado:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el celular del empleado"
          class="form-control"
          name="celular_emp" value=""
          id="celular_emp">
      </div>
      <div class="col-md-4">
          <label for="">Correo de Empleado:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el correo del empleado"
          class="form-control"
          name="correo_emp" value=""
          id="correo_emp">
      </div>
      <div class="col-md-4">
        <label for="">Ciudad del empleado:</label>
        <br>
        <input type="text"
        placeholder="Ingrese la ciudad del empleado"
        class="form-control"
        name="ciudad_emp" value=""
        id="ciudad_emp">
      </div>
    </div>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/empleados/indexE"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>
