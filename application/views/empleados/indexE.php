<h1>Listado de Empleados</h1>
<?php if ($empleados): ?>
  <table class="table table-striped table-bordered table-hover" style="color:black;">
    <thead>
      <tr>
        <th>ID</th>
        <th>NOMBRE DE EMPLEADO</th>
        <th>APELLIDO DE EMPLEADO</th>
        <th>GENERO DE EMPLEADO</th>
        <th>CELULAR DE EMPLEADO</th>
        <th>CORREO DE EMPLEADO</th>
        <th>CIUDAD DE EMPLEADO</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($empleados as $filaTemporal): ?>
        <tr>
          <td>
            <?php echo $filaTemporal->id_empleado; ?>
          </td>
          <td>
            <?php echo $filaTemporal->nom_emp; ?>
          </td>
          <td>
            <?php echo $filaTemporal->ape_emp; ?>
          </td>
          <td>
            <?php echo $filaTemporal->genero_emp; ?>
          </td>
          <td>
            <?php echo $filaTemporal->celular_emp; ?>
          </td>
          <td>
            <?php echo $filaTemporal->correo_emp; ?>
          </td>
          <td>
            <?php echo $filaTemporal->ciudad_emp; ?>
          </td>
          <td class="text-center">
            <a href="#" title="Editar Empleado">
              <i class="glyphicon glyphicon-pencil"></i>
            </a>
            &nbsp; &nbsp; &nbsp;
            <a href="<?php echo site_url(); ?>/Empleados/eliminar/<?php echo $filaTemporal->id_empleado; ?>" title="Eliminar Empleado" style="color:red;">
              <i class="glyphicon glyphicon-trash"></i>
            </a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>

<?php else: ?>
  <h1>NO HAY DATOS</h1>
<?php endif; ?>
