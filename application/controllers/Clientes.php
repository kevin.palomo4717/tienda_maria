<?php

    class Clientes extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
            //cargar un modelp
           $this->load->model('Cliente');

        }



        //Funcion que renderiza la vista index

        public function indexC(){

            $data['clientes']=$this->Cliente->obtenerTodos();
            $this->load->view('header');
            $this->load->view('clientes/indexC',$data);
            $this->load->view('footer');
        }

        //Funcion que renderiza la vista nuevo
        public function nuevoC(){
            $this->load->view('header');
            $this->load->view('clientes/nuevoC');
            $this->load->view('footer');
        }

        public function guardar(){
          $datosNuevosCliente=array("nom_cli"=>$this->input->post('nom_cli'),"ape_cli"=>$this->input->post('ape_cli'),
          "cedula_cli"=>$this->input->post('cedula_cli'),"celu_cli"=>$this->input->post('celu_cli'),"correo_cli"=>$this->input->post('correo_cli'),"pago_cli"=>$this->input->post('pago_cli')
        );
        if($this->Cliente->insertar($datosNuevosCliente)){
          redirect('clientes/indexC');

        }else{
          echo "<h1>ERROR AL INSERTAR</h1>";
        }

     }
     //FUNCION PARA ELIMINAR CONSTRUCTORES ojo
     public function eliminar($id_cli){
       if ($this->Cliente->borrar($id_cli)){
         redirect('clientes/indexC');
         // code...
       } else {
         echo "ERROR AL BORRAR :/";
       }


     }
    }//cierre de la clases NOOOOOOOOOOO BORRRAARRRR

?>
