<?php

    class Empleados extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
            //cargar un modelo
           $this->load->model('Empleado');

        }

        //Funcion que renderiza la vista index

        public function indexE(){
          $data['empleados']=$this->Empleado->obtenerTodos();

            $this->load->view('header');
            $this->load->view('empleados/indexE',$data);
            $this->load->view('footer');
        }

        //Funcion que renderiza la vista nuevo
        public function nuevoE(){
            $this->load->view('header');
            $this->load->view('empleados/nuevoE');
            $this->load->view('footer');
        }

        public function guardar(){
          $datosNuevosEmpleado=array("nom_emp"=>$this->input->post('nom_emp'),"ape_emp"=>$this->input->post('ape_emp'),
          "genero_emp"=>$this->input->post('genero_emp'),"celular_emp"=>$this->input->post('celular_emp'),"correo_emp"=> $this->input->post('correo_emp'),
          "ciudad_emp"=>$this->input->post('ciudad_emp')
        );
        if($this->Empleado->insertar($datosNuevosEmpleado)){
          redirect('empleados/indexE');

        }else{
          echo "<h1>ERROR AL INSERTAR</h1>";
        }

     }
     //FUNCION PARA ELIMINAR CONSTRUCTORES
     public function eliminar($id_empleado){
       if ($this->Empleado->borrar($id_empleado)){
         redirect('empleados/indexE');
         // code...
       } else {
         echo "ERROR AL BORRAR :/";
       }


     }
    }//cierre de la clases NOOOOOOOOOOO BORRRAARRRR

?>
