<?php

    class Productos extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
            //cargar un modelp
           $this->load->model('Producto');

        }



        //Funcion que renderiza la vista index

        public function indexP(){

            $data['productos']=$this->Producto->obtenerTodos();
            $this->load->view('header');
            $this->load->view('productos/indexP',$data);
            $this->load->view('footer');
        }

        //Funcion que renderiza la vista nuevo
        public function nuevoP(){
            $this->load->view('header');
            $this->load->view('productos/nuevoP');
            $this->load->view('footer');
        }

        public function guardar(){
          $datosNuevosProducto=array("nom_pro"=>$this->input->post('nom_pro'),"caducidad_pro"=>$this->input->post('caducidad_pro'),
          "precio_pro"=>$this->input->post('precio_pro'),"tamaño_pro"=>$this->input->post('tamaño_pro'),"dispo_pro"=>$this->input->post('dispo_pro'),"falta_pro"=>$this->input->post('falta_pro')
        );
        if($this->Producto->insertar($datosNuevosProducto)){
          redirect('productos/indexP');

        }else{
          echo "<h1>ERROR AL INSERTAR</h1>";
        }

     }
     //FUNCION PARA ELIMINAR CONSTRUCTORES
     public function eliminar($id_pro){
       if ($this->Producto->borrar($id_pro)){
         redirect('productos/indexP');
         // code...
       } else {
         echo "ERROR AL BORRAR :/";
       }


     }
    }//cierre de la clases NOOOOOOOOOOO BORRRAARRRR

?>
